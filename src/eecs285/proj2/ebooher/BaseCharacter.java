package eecs285.proj2.ebooher;

import static java.lang.Math.sqrt;

public abstract class BaseCharacter {
  int playerIndex;
  double startColumn;
  double startRow;
  double speed;

  public BaseCharacter(int index, double column, double row, double initialSpeed){
    playerIndex=index;
    startColumn=column;
    startRow=row;
    speed=initialSpeed;
  }
  public int getStartColumn(){
    return (int) Math.round(startColumn);
  };
  public int getStartRow(){
    return (int) Math.round(startRow);
  };

  abstract void performMove(Ball ball, char[][] field);

}
