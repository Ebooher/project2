package eecs285.proj2.ebooher;
import static java.lang.Math.sqrt;

class Quarterback extends BaseCharacter{
  double stopColumn;
  double stopRow;
  double throwToColumn;
  double throwToRow;
  double throwSpeed;
  int thrown;
  public Quarterback(int index, double column, double row, double initialSpeed,
                     double sColumn, double sRow, double throwCol,
                     double throwRow, double throwSpd){
  super(index, column, row, initialSpeed);
    stopColumn=sColumn;
    stopRow=sRow;
    throwToColumn=throwCol;
    throwToRow=throwRow;
    throwSpeed=throwSpd;
  }
  public void performMove(Ball ball, char[][] field){
    field[(int)startColumn][(int)startRow]='-';
    if(startColumn==stopColumn&&startRow==stopRow&&thrown==0){
      thrown++;
      throwBall(ball);
    }

    double x=stopColumn-startColumn;
    double y=stopRow-startRow;
    double length=0;
    length=sqrt(x*x+y*y);
    x=(x/length)*speed;
    y=(y/length)*speed;
    startColumn=startColumn+Math.round(x);
    startRow=startRow+Math.round(y);
    if(startColumn-stopColumn>=0&&startRow-stopRow>=0){
      startColumn= stopColumn;
      startRow= stopRow;
    }
    if(thrown==0){
      ball.Travel(startRow,startColumn);
    }
    fieldMove(field);
  }
  private void fieldMove(char[][] field){
    field[(int)startColumn][(int)startRow]='Q';
  }

  private void throwBall(Ball ball){
    ball.Thrown(startColumn,startRow,throwToColumn,throwToRow,throwSpeed);
  }
}

class Receiver extends BaseCharacter{
  double intermediateColumn;
  double intermediateRow;
  double stopColumn;
  double stopRow;
  int movePhase=0;
  public Receiver(int index, double column, double row, double initialSpeed,
                  double interColumn, double interRow, double sColumn,
                  double sRow){
    super(index, column, row, initialSpeed);
    intermediateColumn=interColumn;
    intermediateRow=interRow;
    stopColumn=sColumn;
    stopRow=sRow;
  }

  public void performMove(Ball ball, char[][] field){
    field[(int)startColumn][(int)startRow]='-';
    if(movePhase==0) {
      double x = intermediateColumn - startColumn;
      double y = intermediateRow - startRow;
      double length = 0;
      length = sqrt(x * x + y * y);
      x = (x / length) * speed;
      y = (y / length) * speed;
      startColumn = startColumn + Math.round(x);
      startRow = startRow + Math.round(y);
      if(startColumn-intermediateColumn>=0&&startRow-intermediateRow>=0){
        startColumn= intermediateColumn;
        startRow= intermediateRow;
      }
      if(startColumn==intermediateColumn&&startRow==intermediateRow){
        movePhase++;
      }

      checkCatch(startColumn, startRow, ball);
    }
    else{
      double x = stopColumn - startColumn;
      double y = stopRow - startRow;
      double length = 0;
      length = sqrt(x * x + y * y);
      x = (x / length) * speed;
      y = (y / length) * speed;
      startColumn = startColumn + Math.round(x);
      startRow = startRow + Math.round(y);

      if(startColumn-stopColumn>=0&&startRow-stopRow>=0){
        startColumn= stopColumn;
        startRow= stopRow;
      }
      checkCatch(startColumn, startRow, ball);
    }
    fieldMove(field);
  }
  private void fieldMove(char[][] field){
    field[(int)startColumn][(int)startRow]='R';
  }
  private void checkCatch(double column, double row, Ball ball){
    if(ball.startColumn==column&&ball.startRow==row){
      ball.gameState=3;
    }
  }
}

class Defender extends BaseCharacter{
  public Defender(int index, double column, double row, double initialSpeed){
    super(index, column, row, initialSpeed);
  }
  public void performMove(Ball ball, char[][] field){
    if(ball.gameState==5){
      field[(int)startColumn][(int)startRow]='-';
      double x = ball.destColumn - startColumn;
      double y = ball.destRow - startRow;
      double length = 0;
      length = sqrt(x * x + y * y);
      x = (x / length) * speed;
      y = (y / length) * speed;
      startColumn = startColumn + Math.round(x);
      startRow = startRow + Math.round(y);
      if(startColumn-ball.destColumn>=0&&startRow-ball.destRow>=0){
        startColumn= ball.destColumn;
        startRow= ball.destRow;
      }
      checkIntercept(startColumn,startRow,ball);
    }else if(ball.gameState==0){
      field[(int)startColumn][(int)startRow]='-';
      double x = ball.startColumn - startColumn;
      double y = ball.startRow - startRow;
      double length = 0;
      length = sqrt(x * x + y * y);
      x = (x / length) * speed;
      y = (y / length) * speed;
      startColumn = startColumn + Math.round(x);
      startRow = startRow + Math.round(y);
      if(startColumn-ball.startColumn>=0&&startRow-ball.startRow>=0){
        startColumn= ball.startColumn;
        startRow= ball.startRow;
      }

      checkSack(startColumn,startRow,ball);
    }
    fieldMove(field);
  }
  private void checkSack(double column, double row, Ball ball){
    if(ball.startColumn==column&&ball.startRow==row){
      ball.gameState=1;
    }
  }
  private void fieldMove(char[][] field){
    field[(int)startColumn][(int)startRow]='D';
  }
  private void checkIntercept(double column, double row, Ball ball){
    if(ball.startColumn==column&&ball.startRow==row){
      ball.gameState=2;
    }
  }
}

class Ball{
  double startColumn;
  double startRow;
  double destColumn;
  double destRow;
  double speed;
  int gameState=0; //1=sack, 2=intercept, 3=reception, 4=incomplete, 5=thrown

  public Ball(){}

  public void Travel(double row, double column){
    startColumn=column;
    startRow=row;
  }

  public void Move(){
    double x=destColumn-startColumn;
    double y=destRow-startRow;
    double length=0;
    length=sqrt(x*x+y*y);
    x=(x/length)*speed;
    y=(y/length)*speed;
    startColumn=startColumn+Math.round(x);
    startRow=startRow+Math.round(y);
    if(startColumn-destColumn>=0&&startRow-destRow>=0){
      startColumn= destColumn;
      startRow= destRow;
    }
  }

  public void incompleteCheck(){
    if(startColumn==destColumn&&startRow==destRow){
      gameState=4;
    }
  }

  public void Thrown(double strtColumn, double strtRow, double dstColumn,
                     double dstRow, double thrownSpeed){
    startColumn=strtColumn;
    startRow=strtRow;
    destColumn=dstColumn;
    destRow=dstRow;
    speed=thrownSpeed;
    gameState=5;
  }
}

