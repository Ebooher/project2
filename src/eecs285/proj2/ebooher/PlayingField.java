/**
 * EECS 285 Project 2 - simulation of Tefball game.
 *
 * See https://eecs285.github.io/p2-tefball/ for the specification.
 */

package eecs285.proj2.ebooher; // replace with your uniqname
import static java.lang.System.out;

import java.util.Vector;

 class PlayingField {
  private BaseCharacter [] player=new BaseCharacter[30];
  char [][] field= new char[20][20];
  int playerCount;
  int width;
  int height;
  Ball ball=new Ball();
  /**
   * Creates a PlayingField object.
   *
   * @param numPlayers  the number of players in the game
   * @param fieldWidth  the width of the playing field
   * @param fieldHeight  the height of the playing field
   */
  PlayingField(int numPlayers,
               int fieldWidth,
               int fieldHeight) {
    playerCount=numPlayers;

    width=fieldWidth+2;
    height=fieldHeight+2;

    field[0][0]=' ';

    for(int i=1;i<width;i++) {
      field[i][0]= (char) ((i-1)%10+'0');
    }

    for(int j=1;j<height;j++) {
      field[0][j]= (char) ((j-1)%10+'0');
    }

    for(int i=1;i<width;i++){
      for(int j=1;j<height;j++){
        field[i][j]='-';
      }
    }
    //initialize field with openSpace and boundaries
   /* for(int i=0;i<fieldWidth;i++){
      for(int j=0;j<fieldHeight;j++){
        if(i==0&&j==0){field[i][j]=' ';}
        else if(i<10&&j==0){field[i][j]= (char) (i+'0');}
        else if(i>=10&&j==0){field[i][j]= (char) (i%10+'0');}
        else if(j<10&&i==0){field[i][j]= (char) (j+'0');}
        else if(j>=10&&i==0){field[i][j]= (char) (j%10+'0');}
        else{field[i][j]='-';}
      }
    }*/
  }

  /**
   * Adds a quarterback to the field.
   *
   * @param playerIndex  0-based index that determines when the player
   *                     will move in each turn
   * @param startColumn  the column of the player's starting position
   * @param startRow  the row of the player's starting position
   * @param stopColumn  the column of the player's stopping position
   * @param stopRow  the row of the player's stopping position
   * @param throwToColumn  the column of the location to throw the ball
   * @param throwToRow  the row of the location to throw the ball
   * @param speed  the speed at which the player moves
   * @param throwSpeed  the speed at which the ball moves once it is
   *                    thrown
   */
  void addQuarterback(int playerIndex,
                      double startColumn,
                      double startRow,
                      double stopColumn,
                      double stopRow,
                      double throwToColumn,
                      double throwToRow,
                      double speed,
                      double throwSpeed) {
    BaseCharacter quarter= new Quarterback(playerIndex, startColumn+1, startRow+1,
        speed, stopColumn+1, stopRow+1, throwToColumn+1, throwToRow+1, throwSpeed);
    field[(int) startColumn+1][(int) startRow+1]='Q';
    player[playerIndex]=quarter;
  }

  /**
   * Adds a receiver to the field.
   *
   * @param playerIndex  0-based index that determines when the player
   *                     will move in each turn
   * @param startColumn  the column of the player's starting position
   * @param startRow  the row of the player's starting position
   * @param intermediateColumn  the column of the player's
   *                            intermediate destination
   * @param intermediateRow  the row of the player's intermediate
   *                         destination
   * @param stopColumn  the column of the player's final destination
   * @param stopRow  the row of the player's final destination
   * @param speed  the speed at which the player moves
   */
  void addReceiver(int playerIndex,
                   double startColumn,
                   double startRow,
                   double intermediateColumn,
                   double intermediateRow,
                   double stopColumn,
                   double stopRow,
                   double speed) {
    // your code here
    BaseCharacter receiver= new Receiver(playerIndex, startColumn+1, startRow+1,
        speed, intermediateColumn+1, intermediateRow+1, stopColumn+1, stopRow+1);
    field[(int) startColumn+1][(int) startRow+1]='R';
    player[playerIndex]=receiver;

  }

  /**
   * Adds a defender to the field.
   *
   * @param playerIndex  0-based index that determines when the player
   *                     will move in each turn
   * @param startColumn  the column of the player's starting position
   * @param startRow  the row of the player's starting position
   * @param speed  the speed at which the player moves
   */
  void addDefender(int playerIndex,
                   double startColumn,
                   double startRow,
                   double speed) {
    // your code here
    BaseCharacter defender= new Defender(playerIndex, startColumn+1,
        startRow+1, speed);
    field[(int) startColumn+1][(int) startRow+1]='D';
    player[playerIndex]=defender;
  }

  /**
   * Determines whether or not the game setup is valid.
   *
   * A valid game has a single quarterback and a player added to each
   * expected index.
   *
   * @return  whether or not the game is valid
   */
  boolean checkIsValidGame() {
    int quarterCount=0;
    int playerNum=0;
    for(int i=0;i<player.length;i++) {
      if(player[i]==null){break;}
      if (player[i].toString().contains("Quarterback")||
          player[i].toString().contains("Defender") ||
          player[i].toString().contains("Receiver") ) {
        playerNum++;
        if(player[i].toString().contains("Quarterback")){quarterCount++ ;}
      }
    }
    if(quarterCount==1&&playerNum==playerCount){
    return true;
  }else {
      return false; // replace with your solution
    }
  }

  void printGame(){
    for(int i=0;i<height;i++){
      for(int j=0;j<width;j++){
        out.print(field[j][i]);
      }
      out.println();
    }
  }

  /**
   * Runs the game simulation to completion, returning the result.
   *
   * Simulates turns until the game ends. A turn involves the ball
   * moving first, if it has been thrown, and then each player in turn
   * by index. If a game-ending event occurs during a turn (i.e. a
   * sack, interception, reception, or incompletion), returns with the
   * corresponding enum value. Otherwise, continues to simulate turns
   * until the game ends with one of those events.
   *
   * Note: a turn may not complete - if a game-ending event occurs,
   * the game ends immediately without the remaining players moving.
   *
   * @return  enum value corresponding to the result of the game
   */
  GameResultEnum playBall() {
    do {
      if (ball.gameState==1) {
        return GameResultEnum.SACK;
      } else if (ball.gameState==2) {
        return GameResultEnum.INTERCEPTION;
      } else if (ball.gameState==3) {
        return GameResultEnum.RECEPTION;
      } else if (ball.gameState==4) {
        return GameResultEnum.INCOMPLETION;
      }else{
        if(ball.gameState==5){
          field[(int)ball.startColumn][(int)ball.startRow]='-';
          ball.Move();
          field[(int)ball.startColumn][(int)ball.startRow]='B';
          ball.incompleteCheck();
        }

        for(int i=0;i<playerCount;i++){
          player[i].performMove(ball,field);
        }
        printGame();
      }
    }while(true);
  }
}
